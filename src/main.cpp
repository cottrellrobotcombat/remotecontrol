#include "SH1106SPi.h"

#include <esp_now.h>
#include <WiFi.h>

#include "bargraph.h"
#include "mac_addresses.h"

BarGraph* leftMotorBar  = NULL;
BarGraph* leftArmBar    = NULL;
BarGraph* rightMotorBar = NULL;
BarGraph* rightArmBar   = NULL;

constexpr size_t MAC_LENGTH = 6;

const uint8_t* MY_MAC = REMOTE_MAC;
const uint8_t* PEER_MAC = ROBOT_MAC;
esp_now_peer_info_t peerInfo;

const int DISPLAY_SPI_RES = 17;
const int DISPLAY_SPI_DC = 16;
const int DISPLAY_SPI_CS = 5;

const int JOYSTICK_LEFT_X = 33;
const int JOYSTICK_LEFT_Y = 32;
const int JOYSTICK_RIGHT_X = 34;
const int JOYSTICK_RIGHT_Y = 35;

const int JOYSTICK_OFFSET_LEFT_X = -187;
const int JOYSTICK_OFFSET_LEFT_Y = -208;
const int JOYSTICK_OFFSET_RIGHT_X = -197;
const int JOYSTICK_OFFSET_RIGHT_Y = -162;

const int JOYSTICK_LEFT_CLICK = 26;
const int JOYSTICK_RIGHT_CLICK = 25;

SH1106Spi display(DISPLAY_SPI_RES, DISPLAY_SPI_DC, DISPLAY_SPI_CS);

struct ControlMessage {
  int8_t leftMotor = 0;
  int8_t rightMotor = 0;
  int8_t leftArm = 0;
  int8_t rightArm = 0;
  bool enable = false;
  uint8_t sync = 0;
};

struct ReportMessage {
  uint16_t batteryMillivolts = 0;
  bool isEnabled = false;
  uint8_t sync = 0;
};

ControlMessage outgoingMessage;
ReportMessage incomingMessage;

constexpr size_t INCOMING_LENGTH = sizeof(incomingMessage);
constexpr size_t OUTGOING_LENGTH = sizeof(outgoingMessage);

void onDataTx(const uint8_t* mac, esp_now_send_status_t status) {
  Serial.print("Last message callback status: ");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "SUCCESS" : "FAIL");
}

unsigned long millisAtLastRx = 0;

void onDataRx(const uint8_t* mac, const uint8_t* incomingData, int len) {
  if (len == INCOMING_LENGTH) {
    memcpy(&incomingMessage, incomingData, INCOMING_LENGTH);
    millisAtLastRx = millis();
  }
  else {
    Serial.print("Unexpected incoming datasize: ");
    Serial.print(len);
    Serial.print(", expected ");
    Serial.print(INCOMING_LENGTH);
  }
}

bool checkMacAddressIs(const uint8_t* ref) {
  // Memory allocation (bytes and string)
  uint8_t mac[MAC_LENGTH];
  char macStr[MAC_LENGTH*3] = { 0 };
  // Get system MAC addr, print it and check against ref
  WiFi.macAddress(mac);
  sprintf(macStr, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
  Serial.print("My MAC is: ");
  Serial.println(String(macStr));
  bool mismatch = false;
  for (size_t i = 0; i < MAC_LENGTH; ++i)
    mismatch |= mac[i] != ref[i];
  // If didn't match, print the expectation too
  if (mismatch) {
    sprintf(macStr, "%02X:%02X:%02X:%02X:%02X:%02X", ref[0], ref[1], ref[2], ref[3], ref[4], ref[5]);
    Serial.print("Expected:  ");
    Serial.println(String(macStr));
  }
  return !mismatch;
}

void setup() {
  Serial.begin(115200);
  Serial.print("\n\n");

  Serial.print("Incoming length: ");
  Serial.println(INCOMING_LENGTH);
  Serial.print("Outgoing length: ");
  Serial.println(OUTGOING_LENGTH);

  // Initialising the UI will init the display too.
  display.init();

  // display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
  display.setTextAlignment(TEXT_ALIGN_LEFT);

  adcAttachPin(JOYSTICK_LEFT_X);
  adcAttachPin(JOYSTICK_LEFT_Y);
  adcAttachPin(JOYSTICK_RIGHT_X);
  adcAttachPin(JOYSTICK_RIGHT_Y);

  pinMode(JOYSTICK_LEFT_CLICK, INPUT_PULLUP);
  pinMode(JOYSTICK_RIGHT_CLICK, INPUT_PULLUP);

  WiFi.mode(WIFI_STA);
  checkMacAddressIs(MY_MAC);
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initialising ESP NOW");
    return;
  }
  else {
    Serial.println("Successfully init ESP NOW");
  }

  esp_now_register_send_cb(onDataTx);
  esp_now_register_recv_cb(onDataRx);

  memcpy(peerInfo.peer_addr, PEER_MAC, 6);
  peerInfo.channel = 0;
  peerInfo.encrypt = false;

  if (esp_now_add_peer(&peerInfo) != ESP_OK) {
    Serial.println("Failed to add peer");
  }
  else {
    Serial.println("Added peer successfully");
  }

  leftMotorBar  = new BarGraph(  1, 33,  8, 30, 127, -127);
  leftArmBar    = new BarGraph(  1,  0,  8, 30, 127, -127);
  rightMotorBar = new BarGraph(118, 33,  8, 30, 127, -127);
  rightArmBar   = new BarGraph(118,  0,  8, 30, 127, -127);
}


// Map a 0 to 4096 (10bit) analog reading from the ADC
// into a -1 to 1 float 
float stickMapping(int analog, int offset) {
  
  const int JOYSTICK_ADC_MAX = (1 << 12) - 1;
  int joystickAdcMid = (1 << 11) + offset;

  float diffFromCentre = analog - joystickAdcMid;

  if (analog > joystickAdcMid)
    return diffFromCentre / float(JOYSTICK_ADC_MAX - joystickAdcMid);
  else
    return diffFromCentre / float(joystickAdcMid);

  return int8_t(round(diffFromCentre * 0x7F));
}

float clamp(float in, float lower, float upper) {
  return (in < lower) ? lower : ((in > upper) ? upper : in);
}

int8_t stickToDrive(float stick) {
  return round(stick * 0x7F);
}

void mixer(const int upVal, const int sideVal, int& spdTgtL, int& spdTgtR) {
  if (upVal == 0 && sideVal != 0) {
    spdTgtL = sideVal;
    spdTgtR = -sideVal;
  } else {
    spdTgtL = upVal * ((-127 - sideVal) / -127.0F);
    spdTgtR = upVal * ((127 - sideVal) / 127.0F);

    if (upVal > 0 && spdTgtL > upVal)
      spdTgtL = upVal;
    if (upVal > 0 && spdTgtR > upVal)
      spdTgtR = upVal;
    if (upVal < 0 && spdTgtL < upVal)
      spdTgtL = upVal;
    if (upVal < 0 && spdTgtR < upVal)
      spdTgtR = upVal;
  }
}

const int DELAY_PER_LOOP_US = 20000;
const int MINIMUM_RX_DROPOUT_MS = 500;

// For text going onto screen
static char tmpBuff[32];

void loop() {

  // Read joysticks
  float leftX  = stickMapping(analogRead(JOYSTICK_LEFT_X), JOYSTICK_OFFSET_LEFT_X);
  float leftY  = stickMapping(analogRead(JOYSTICK_LEFT_Y), JOYSTICK_OFFSET_LEFT_Y);
  float rightX = stickMapping(analogRead(JOYSTICK_RIGHT_X), JOYSTICK_OFFSET_RIGHT_X);
  float rightY = stickMapping(analogRead(JOYSTICK_RIGHT_Y), JOYSTICK_OFFSET_RIGHT_Y);
  bool leftClick  = !digitalRead(JOYSTICK_LEFT_CLICK);
  bool rightClick = !digitalRead(JOYSTICK_RIGHT_CLICK);
  
  int spdTgtL, spdTgtR, posArmL, posArmR;

  mixer(stickToDrive(rightY), stickToDrive(rightX), spdTgtL, spdTgtR);
  mixer(stickToDrive(leftY), stickToDrive(leftX), posArmL, posArmR);

  // Pack Message  
  outgoingMessage.leftMotor = spdTgtL;
  outgoingMessage.rightMotor = spdTgtR;
  outgoingMessage.leftArm = posArmL;
  outgoingMessage.rightArm = posArmR;
  outgoingMessage.enable = true;
  outgoingMessage.sync++;

  // Send Message
  esp_err_t result = esp_now_send(PEER_MAC, (uint8_t *) &outgoingMessage, sizeof(outgoingMessage));
  if (result != ESP_OK) {
    Serial.println("Sending failed");
  }

  // Update bar graph states
  leftMotorBar->update(outgoingMessage.leftMotor);
  leftArmBar->update(outgoingMessage.leftArm);
  rightMotorBar->update(outgoingMessage.rightMotor);
  rightArmBar->update(outgoingMessage.rightArm);

  // GUI
  display.clear();

  display.drawString(41,0,"BitsBotRC");

  if (millis() > millisAtLastRx + MINIMUM_RX_DROPOUT_MS) {
    display.drawString(35,30, "NO COMMS");
  }
  else {
    display.drawStringf(35,20,tmpBuff, "%d mV", incomingMessage.batteryMillivolts);
    display.drawString(35,30, incomingMessage.isEnabled ? "Enabled" : "Disabled");
  }

  // Show comm sync values
  display.drawStringf(45,50, tmpBuff, "%03d/%03d", outgoingMessage.sync, incomingMessage.sync);

  // Control numbers
  display.drawStringf(16, 42, tmpBuff, "%+04d", outgoingMessage.leftMotor);
  display.drawStringf(16,  8, tmpBuff, "%+04d", outgoingMessage.leftArm);
  display.drawStringf(88, 42, tmpBuff, "%+04d", outgoingMessage.rightMotor);
  display.drawStringf(88,  8, tmpBuff, "%+04d", outgoingMessage.rightArm);
  
  // Control bar graphs
  display.fillRect(leftMotorBar->x(), leftMotorBar->y(), leftMotorBar->w(), leftMotorBar->h());
  display.fillRect(leftArmBar->x(), leftArmBar->y(), leftArmBar->w(), leftArmBar->h());
  display.fillRect(rightMotorBar->x(), rightMotorBar->y(), rightMotorBar->w(), rightMotorBar->h());
  display.fillRect(rightArmBar->x(), rightArmBar->y(), rightArmBar->w(), rightArmBar->h());

  // Tick lines
  display.drawLine(10, leftMotorBar->tickHeight(), 13, leftMotorBar->tickHeight());
  display.drawLine(10, leftArmBar->tickHeight(), 13, leftArmBar->tickHeight());
  display.drawLine(113, rightMotorBar->tickHeight(), 116, rightMotorBar->tickHeight());
  display.drawLine(113, rightArmBar->tickHeight(), 116, rightArmBar->tickHeight());

  display.display();

  // Pause before next loop
  delayMicroseconds(DELAY_PER_LOOP_US);

}

#include <stdint.h>
#include <cmath>

// NB: (0,0) is Top,Left 

class BarGraph {

public:
    BarGraph(int16_t x, int16_t y, int16_t w, int16_t h, float min, float max) 
    : _x(x), _w(w) {
        this->_gain = float(h) / (max - min);
        this->_zero = float(y) - (min * this->_gain);
    }

    void update(float val) {
        this->_sz = round(val * this->_gain);
        if (this->_sz >= 0) {
            this->_sz++; // So a meas of 0 gives a centred line
        }
    }

    int16_t tickHeight() { return this->_zero; }

    int16_t x() { return this->_x; }
    int16_t y() { return (this->_sz < 0) ? (this->_zero + this->_sz) : this->_zero; }
    int16_t w() { return this->_w; }
    int16_t h() { return (this->_sz < 0) ? (-this->_sz) : this->_sz; }

private:

    int16_t _sz;

    int16_t _x;
    int16_t _w;

    int16_t _zero;
    float _gain;

};